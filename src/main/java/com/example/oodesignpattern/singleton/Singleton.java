package com.example.oodesignpattern.singleton;

//Singleton is a class that only has one instance and provides global point of access to it
public class Singleton {
    private static Singleton instance;

    private Singleton() {
    }

    public static Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }


}
