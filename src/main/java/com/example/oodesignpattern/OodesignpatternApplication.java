package com.example.oodesignpattern;

import com.example.oodesignpattern.strategy.Context;
import com.example.oodesignpattern.strategy.OpeartionSubstract;
import com.example.oodesignpattern.strategy.OperationAdd;
import decoratorpattern.Circle;
import decoratorpattern.Rectangle;
import decoratorpattern.RedShapeDecorator;
import decoratorpattern.Shape;
import factorypattern.Demo;
import factorypattern.ShapeFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.SQLOutput;

@SpringBootApplication
public class OodesignpatternApplication {

    public static void main(String[] args) {

        SpringApplication.run(OodesignpatternApplication.class, args);

        Demo.demo();



        //runDecoratorPattern();

        //runStrategyDesignPattern();


    }

    private static void runDecoratorPattern() {
        Shape circle = new Circle();
        Shape redCircle = new RedShapeDecorator(new Circle());

        Shape redRectangle = new RedShapeDecorator(new Rectangle());

        System.out.println("Circle with normal border");
        circle.draw();

        System.out.println("\nCircle of red border");
        redCircle.draw();

        System.out.println("\nRectangle of red border");
        redRectangle.draw();
    }

    private static void runStrategyDesignPattern() {

        Context context = new Context(new OperationAdd());
        System.out.println("10 + 5 = " + context.executeStrategy(10,5));

        context= new Context(new OpeartionSubstract());
        System.out.println("10 - 5 = " + context.executeStrategy(10,5));

        context = new Context(new OpeartionSubstract());
        System.out.println("10 * 5 = " + context.executeStrategy(10,5));
    }

}
