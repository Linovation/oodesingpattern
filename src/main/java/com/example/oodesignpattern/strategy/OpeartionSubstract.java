package com.example.oodesignpattern.strategy;

public class OpeartionSubstract implements Strategy {
    @Override
    public int doOperation(int num1, int num2) {
        return num1 - num2;
    }
}
